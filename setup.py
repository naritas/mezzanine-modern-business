#!/usr/bin/env python

from setuptools import setup, find_packages

import os

if os.path.exists("README.txt"):
    readme = open("README.txt")
else:
    print "Warning: using markdown README"
    readme = open("README.md")

setup(
    name="mezzanine-modern-business",
    version="0.4",
    description="Integration template Modern Business with mezzanine",
    long_description=readme.read(),
    author="Luis Velez",
    author_email="lvelezsantos@gmail.com",
    license="BSD",
    url="https://bitbucket.org/naritas/mezzanine-modern-business",
    install_requires=(
        "mezzanine >= 3",
        "south",
    ),
    packages=['mezzanine_modern_business'],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
    ],
)
