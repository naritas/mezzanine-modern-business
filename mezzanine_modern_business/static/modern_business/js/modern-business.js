// Activates the Carousel
$('.carousel').carousel({
  interval: 5000
});

// Activates Tooltips for Social Links
$('.tooltip-social').tooltip({
  selector: "a[data-toggle=tooltip]"
});

$(document).ready(function(){
   $('input[type=text]').addClass('form-control');
   $('input[type=email]').addClass('form-control');
   $('textarea').addClass('form-control');
});